Stream Viewer
=============

A small utility that hides in the taskbar and notifies you of new streams from your favorite channels.

Build Setup
-----------

Requirements:

* Java 7
* Eclipse
* Maven
