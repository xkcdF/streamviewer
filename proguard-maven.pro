-injars         target/<project.artifactId>-<project.version>-jar-with-dependencies.jar(!ch/**,!org/slf4j/**,!com/mercutio/streams/test/**,!org/junit/**,!junit/**,!org/w3c/**,!org/xml/**,!javax/xml/**)
-libraryjars    proguardlibs/jsr305.jar
-libraryjars    <java.home>/lib/rt.jar
-outjars        target/<project.artifactId>-<project.version>.jar

-dontoptimize
-dontobfuscate
-dontnote com.google.**
-dontnote org.ibex.nestedvm.util.Platform
-dontnote org.sqlite.**
-dontnote org.jfree.chart.**
-dontnote org.jfree.data.time.RegularTimePeriod
-dontwarn sun.misc.Unsafe
-dontwarn com.google.common.collect.MinMaxPriorityQueue
-dontwarn com.google.common.util.concurrent.ServiceManager
-dontwarn org.jooq.impl.Utils
-dontwarn org.jooq.tools.JooqLogger
-dontwarn org.ibex.nestedvm.ClassFileCompiler
-dontwarn org.jfree.chart.servlet.**

-keep public class com.mercutio.streams.Main {
    public static void main(java.lang.String[]);
}

-keepclassmembers enum * {
    public static **[] values();
    public static ** valueOf(java.lang.String);
}

-keep class org.ibex.**
-keep class org.sqlite.**