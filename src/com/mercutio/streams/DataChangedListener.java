package com.mercutio.streams;

import java.util.EventListener;

public interface DataChangedListener extends EventListener {
	void updateData(DataProvider provider);
}
