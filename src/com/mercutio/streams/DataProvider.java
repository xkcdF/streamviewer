package com.mercutio.streams;

import java.util.Collection;
import java.util.Set;

import com.mercutio.streams.twitch.api.Stream;

public interface DataProvider {
	Collection<Stream> getStreams();

	Set<Integer> getFavoriteStreams();

	Set<String> getFavoriteGames();
}