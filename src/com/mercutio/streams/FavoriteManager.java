package com.mercutio.streams;

import com.mercutio.streams.browsers.Browser;

public interface FavoriteManager {
	void setFavoriteStream(Integer id, boolean favorite);

	void setFavoriteGame(String game, boolean favorite);
	
	void setFavoriteBrowser(Browser browser);
	Browser getFavoriteBrowser();
}
