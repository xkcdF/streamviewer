package com.mercutio.streams;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.NumberFormat;
import java.util.Vector;
import java.util.regex.Pattern;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;

public class FavoriteTableUtil {

	private static final String REBROADCAST = "\\[?re(?:broadcast|run)\\]?";

	public static JTable createFavoriteTable(Container c, final TableClickListener listener, final TooltipProvider tooltips, Vector<Vector<Object>> data, Vector<String> columns, final ImageIcon favorite, final ImageIcon notFavorite) {
		final DefaultTableModel tableModel = new DefaultTableModel(data, columns) {
			private static final long serialVersionUID = 1L;

			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}

			@Override
			public Class<?> getColumnClass(int columnIndex) {
				if (columnIndex == 0)
					return Boolean.class;
				if (columnIndex == 3)
					return Integer.class;
				return super.getColumnClass(columnIndex);
			}

		};

		final JTable table = new JTable(tableModel);
		table.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		table.setFocusable(false);
		table.getTableHeader().setReorderingAllowed(false);
		table.addMouseMotionListener(new MouseAdapter() {
			@Override
			public void mouseMoved(MouseEvent e) {
				table.getSelectionModel().setSelectionInterval(0, table.rowAtPoint(e.getPoint()));
			}
		});

		if (listener != null)
			table.addMouseListener(new MouseAdapter() {
				@Override
				public void mouseReleased(MouseEvent e) {
					int row = table.getSelectedRow();
					if (row == table.rowAtPoint(e.getPoint())) {
						int col = table.columnAtPoint(e.getPoint());
						if (col >= 0)
							listener.tableClicked(tableModel, row, col, e);
					}
				}
			});

		table.setDefaultRenderer(Object.class, new DefaultTableCellRenderer() {
			private static final long serialVersionUID = 1L;

			private Color foreground;

			@Override
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
				if (value instanceof Number) {
					setHorizontalAlignment(SwingConstants.TRAILING);
					NumberFormat formatter = NumberFormat.getIntegerInstance();
					formatter.setGroupingUsed(true);
					value = formatter.format(value);
				} else {
					setHorizontalAlignment(SwingConstants.LEADING);
				}

				super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

				boolean inactive = false;
				if (tooltips != null) {
					String tooltip = tooltips.getTooltip(row);
					setToolTipText(tooltip);
					inactive = tooltip != null && isInactive(tooltip.toLowerCase());
				}

				if (foreground == null) {
					foreground = getForeground();
				}
				setForeground(inactive ? Color.DARK_GRAY : foreground);

				return this;
			}
			
			// if it begins or ends with "rebroadcast" / "rerun"
			private final Pattern inactive = Pattern.compile("(?:" + REBROADCAST + ".*|.*" + REBROADCAST + ")", Pattern.CASE_INSENSITIVE);
			private boolean isInactive(String tooltip) {
				return inactive.matcher(tooltip).matches();
			}
		});

		table.setDefaultRenderer(Boolean.class, new DefaultTableCellRenderer() {
			private static final long serialVersionUID = 1L;

			@Override
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
				super.getTableCellRendererComponent(table, "", isSelected, hasFocus, row, column);

				if (value instanceof Boolean) {
					Boolean val = (Boolean) value;
					setIcon(val ? favorite : notFavorite);
					setToolTipText((val ? "Remove from" : "Add to") + " favorites");
				} else {
					setValue(value);
				}

				return this;
			}
		});
		TableColumn first = table.getColumnModel().getColumn(0);
		first.setHeaderRenderer(new TableCellRenderer() {
			@Override
			public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
				Component c = table.getTableHeader().getDefaultRenderer().getTableCellRendererComponent(table, "", isSelected, hasFocus, row, column);
				if (c instanceof JLabel) {
					((JLabel) c).setIcon(favorite);
				}
				return c;
			}
		});
		first.setMinWidth(29);
		first.setMaxWidth(29);

		c.add(new JScrollPane(table));
		
		tableModel.addTableModelListener(new TableModelListener() {
			@Override
			public void tableChanged(TableModelEvent e) {
				if(! table.isShowing())
					return;
				
				Point location = MouseInfo.getPointerInfo().getLocation();
				Point comp = table.getLocationOnScreen();
				location.x -= comp.x;
				location.y -=  comp.y;
				
				int row = table.rowAtPoint(location);
				
				if(row < 0 || table.columnAtPoint(location) < 0) {
					row = table.getSelectionModel().getMinSelectionIndex();
					if(row < 0)
						return;
				}

				// we need to select the row after this event has been processed
				final int selection = row;
				SwingUtilities.invokeLater(new Runnable() {
					@Override
					public void run() {
						table.getSelectionModel().setSelectionInterval(selection, selection);
					}
				});
			}
		});

		return table;
	}

}
