package com.mercutio.streams;

import java.awt.Image;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.swing.ImageIcon;

import com.google.common.collect.ImmutableList;

public class ImageProducer {
	public static ImageIcon getImage(String filename) {
		URL url = ImageProducer.class.getResource("/" + filename);
		if (url != null)
			return new ImageIcon(url);

		return new ImageIcon(filename);
	}

	public static List<Image> getImages(List<String> filenames) {
		List<Image> images = new ArrayList<>();
		for (String image : filenames) {
			images.add(getImage(image).getImage());
		}
		return images;
	}

	public static final String IMG_FAVORITE = "img/heart.png";
	public static final String IMG_NON_FAVORITE = "img/heart_disabled.png";
	public static final String IMG_TRAY_ICON = "img/app-icons/television-16x16.png";
	public static final List<String> IMG_TRAY_ICONS = ImmutableList.of(
		"img/app-icons/television-16x16.png",
		"img/app-icons/television-32x32.png",
		"img/app-icons/television-48x48.png",
		"img/app-icons/television-256x256.png");
}
