package com.mercutio.streams;

import java.awt.AWTEvent;
import java.awt.AWTException;
import java.awt.CheckboxMenuItem;
import java.awt.Menu;
import java.awt.MenuItem;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.AWTEventListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;

import javax.swing.SwingWorker;
import javax.swing.Timer;
import javax.swing.ToolTipManager;
import javax.swing.UIManager;
import javax.swing.UIManager.LookAndFeelInfo;

import com.google.common.collect.Sets;
import com.mercutio.streams.browsers.Browser;
import com.mercutio.streams.twitch.TwitchConnector;
import com.mercutio.streams.twitch.api.Stream;

/**
 * 
 * @author Panultimate
 *
 */
public class Main {

	private final class WindowHider implements WindowFocusListener, AWTEventListener {
		// how many seconds until the streamviewer hides itself
		private static final int HIDE_SECONDS = 5;

		private int remaining = HIDE_SECONDS;

		private final Timer hide = new Timer(1000, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				if (remaining < 1) {
					if (hasStreamViewer()) {
						streamViewer.dispose();
						trayWatcher.removeDataChangedListener(streamViewer);
						streamViewer = null;
					}
					hide.stop();
				} else if (remaining < 4 && hasStreamViewer()) {
					streamViewer.setTitle("StreamViewer (" + remaining + ")");
				}
				remaining--;
			}

		});

		private boolean hasStreamViewer() {
			return streamViewer != null && streamViewer.isDisplayable();
		}

		@Override
		public void windowLostFocus(WindowEvent e) {
			cancel();
			hide.start();
		}

		@Override
		public void windowGainedFocus(WindowEvent e) {
			cancel();
		}

		private void cancel() {
			remaining = HIDE_SECONDS;
			if (hasStreamViewer()) {
				streamViewer.setTitle("StreamViewer");
			}
			if (hide.isRunning()) {
				hide.stop();
			}
		}

		@Override
		public void eventDispatched(AWTEvent event) {
			if (event.getID() == MouseEvent.MOUSE_ENTERED) {
				cancel();
			} else if (event.getID() == MouseEvent.MOUSE_EXITED && !isWindowActive()) {
				cancel();
				hide.start();
			}
		}

		private boolean isWindowActive() {
			for (Window w : Window.getWindows()) {
				if (w.isActive())
					return true;
			}
			return false;
		}
	}

	private static final int REFRESHER_DELAY = 5 * 60 * 1000;

	private final Timer refresher;

	private SettingsDialog settingsDialog;
	private StreamViewer streamViewer;
	private TrayWatcher trayWatcher;
	
	private SwingWorker<Void, Stream> worker;

	private final WindowHider hideViewer;

	public Main() throws AWTException {
		// basic settings

		try {
			for (LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
				if ("Nimbus".equals(info.getName())) {
					UIManager.setLookAndFeel(info.getClassName());
					break;
				}
			}
		} catch (Exception ignore) {
			// use the default l&f
		}

		ToolTipManager.sharedInstance().setInitialDelay(200);

		trayWatcher = new TrayWatcher();

		MenuItem exit = new MenuItem("Exit");
		exit.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				exit();
			}
		});
		trayWatcher.getPopupMenu().add(exit);
		
		final Menu browser = new Menu("Set Browser");

		Browser favorite = trayWatcher.getFavoriteBrowser();
		final CheckboxMenuItem[] checkItems = new CheckboxMenuItem[Browser.values().length];
		ItemListener itemListener = new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				Object source = e.getSource();
				if(((CheckboxMenuItem) source).getState()) {
					for(int i=0; i < checkItems.length; i++) {
						CheckboxMenuItem item = checkItems[i];
						boolean match = source == item;
						if(item.getState() != match) {
							item.setState(match);
						}
						item.setEnabled(!item.getState());
						if(match)
							trayWatcher.setFavoriteBrowser(Browser.values()[i]);
					}
				}
			}
		};
		
		for (int i = 0; i < Browser.values().length; i++) {
			Browser b = Browser.values()[i];
			CheckboxMenuItem bItem = new CheckboxMenuItem(b.toString());
			bItem.setState(b == favorite);
			bItem.setEnabled(b != favorite);
			bItem.addItemListener(itemListener);
			checkItems[i] = bItem;
			browser.add(bItem);
		}
		
		trayWatcher.getPopupMenu().insert(browser, 0);

		SystemTray.getSystemTray().add(trayWatcher);

		trayWatcher.setCombinedListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				showStreamViewer();
			}
		});

		final MenuItem settings = new MenuItem("Settings");
		settings.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (settingsDialog == null) {
					settingsDialog = new SettingsDialog(trayWatcher);
					settingsDialog.addWindowFocusListener(hideViewer);
					trayWatcher.addDataChangedListener(settingsDialog);
				}
				if (trayWatcher.getStreams() != null)
					settingsDialog.updateData(trayWatcher);
				settingsDialog.setVisible(true);
			}
		});
		trayWatcher.getPopupMenu().insert(settings, 0);

		final MenuItem refresh = new MenuItem("Refresh");
		trayWatcher.getPopupMenu().insert(refresh, 0);

		final ActionListener refreshAction = new ActionListener() {
			boolean refreshing;
			int pages;

			private Set<Integer> toSet(Collection<Stream> streams) {
				Set<Integer> r = new HashSet<>();
				if (streams != null) {
					for (Stream s : streams) {
						r.add(s.getChannel().get_id());
					}
				}
				return r;
			}

			@Override
			public void actionPerformed(ActionEvent e) {
				if (refreshing || worker != null && !worker.isDone())
					return;
				
				refreshing = true;

				refresh.setEnabled(false);
				refresh.setLabel("Refreshing...");
				trayWatcher.setToolTip("Refreshing...");

				final Set<Integer> before = toSet(trayWatcher.getStreams());
				final Set<Stream> verification = new HashSet<>();

				worker = new SwingWorker<Void, Stream>() {
					int current = 0;
					
					@Override
					protected Void doInBackground() throws Exception {
						TwitchConnector con = new TwitchConnector();

						while (!isCancelled() && con.hasNext()) {
							current++;
							List<Stream> streams = con.loadPage();
							// show in GUI
							publish(streams.toArray(new Stream[streams.size()]));
							// save to database
							trayWatcher.getDatabaseManager().save(streams);
						}
						con.logTotalTime();
						return null;
					}

					@Override
					protected void process(List<Stream> chunks) {
						verification.addAll(chunks);
						
						List<Stream> streams = filter(toSet(chunks));
						streams.addAll(chunks);

						trayWatcher.setStreams(streams, streamViewer != null);
						String msg = "Refreshing... " + current;
						if(pages > 0) msg += " / " + pages;
						trayWatcher.setToolTip(msg);
					}

					/**
					 * Returns the current streams of the <code>TrayWatcher</code>
					 * excluding the streams that are in the filter.
					 */
					private List<Stream> filter(Set<Integer> filter) {

						List<Stream> filtered = new ArrayList<>();

						Collection<Stream> streams = trayWatcher.getStreams();
						if (streams != null) {
							for (Stream stream : streams) {
								if (!filter.contains(stream.getChannel().get_id()))
									filtered.add(stream);
							}
						}
						
						before.removeAll(filter);

						return filtered;
					}

					@Override
					protected void done() {
						try {
							get();

							if(!before.isEmpty()) {
								System.out.println(before.size() + " streams left over");
								trayWatcher.setStreams(filter(before), streamViewer != null);
							} else {
								System.out.printf("No streams left over (currently %d streams online)\n", trayWatcher.getStreams().size());
							}
							pages = current;
							verify();
						} catch(CancellationException expected) {
						} catch (ExecutionException e) {
							System.err.println("Error getting streams: ");
							e.printStackTrace();
							trayWatcher.setToolTip("An error ocurred: " + e.getCause().getMessage() + " (" + e.getCause().getClass() + ")");
						} catch (InterruptedException e) {
							e.printStackTrace();
							refresh.setLabel("An error ocurred: " + e.getMessage());
						} finally {
							if(trayWatcher.databaseManager != null)
								trayWatcher.databaseManager.done();
							refreshing = false;
							refresh.setEnabled(true);
							refresh.setLabel("Refresh Now");
						}
					}

					private void verify() {
						Set<Integer> streams = toSet(trayWatcher.getStreams());
						Set<Integer> verify = toSet(verification);
						
						if(!verify.equals(streams)) {
							System.out.printf("Verification failed (%d expected, %d contained)\n", verify.size(), streams.size());
							for(Integer id : Sets.symmetricDifference(verify, streams)) {
								if(verify.contains(id))
									System.out.print('-');
								if(streams.contains(id))
									System.out.print('+');
								System.out.println(id);
							}
						}
					}
				};
				worker.execute();
			}
		};
		refresher = new Timer(0, refreshAction);
		refresher.setDelay(REFRESHER_DELAY);
		refresher.start();
		refresh.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if (streamViewer != null && streamViewer.isVisible())
					showStreamViewer();
				refreshAction.actionPerformed(e);
			}
		});

		hideViewer = new WindowHider();
		Toolkit.getDefaultToolkit().addAWTEventListener(hideViewer, MouseEvent.MOUSE_ENTERED | MouseEvent.MOUSE_EXITED);

	}

	private void showStreamViewer() {
		if (streamViewer == null) {
			System.out.println("Instantiating Streamviewer...");
			streamViewer = new StreamViewer(trayWatcher);
			streamViewer.addWindowFocusListener(hideViewer);
			if (trayWatcher.getStreams() != null) {
				streamViewer.updateData(trayWatcher);
			}
			trayWatcher.addDataChangedListener(streamViewer);
		}
		streamViewer.setVisible(true);
	}

	private void exit() {
		if (streamViewer != null && streamViewer.isDisplayable()) {
			streamViewer.dispose();
			streamViewer = null;
		}
		if (settingsDialog != null && settingsDialog.isDisplayable()) {
			settingsDialog.dispose();
			settingsDialog = null;
		}
		SystemTray.getSystemTray().remove(trayWatcher);
		if (refresher.isRunning())
			refresher.stop();
		if(worker != null && !worker.isDone()) {
			worker.cancel(true);
			try {
				worker.get();
			} catch(CancellationException expected) {
			} catch (InterruptedException | ExecutionException e) {
				System.err.println("Error shutting down worker: ");
				e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) throws AWTException {
		if (!SystemTray.isSupported()) {
			return;
		}

		new Main();
	}
}
