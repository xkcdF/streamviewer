package com.mercutio.streams;

public class NumberIconType {
	public final int favs;
	public final boolean unupdated;

	public NumberIconType(int favs, boolean unupdated) {
		this.favs = favs;
		this.unupdated = unupdated;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + favs;
		result = prime * result + (unupdated ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NumberIconType other = (NumberIconType) obj;
		if (favs != other.favs)
			return false;
		if (unupdated != other.unupdated)
			return false;
		return true;
	}
}