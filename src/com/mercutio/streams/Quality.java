package com.mercutio.streams;

public enum Quality {
	BEST, SOURCE, HIGH, MEDIUM, LOW, MOBILE_HIGH, MOBILE_MEDIUM, MOBILE_LOW, MOBILE_MOBILE, WORST
}
