package com.mercutio.streams;

import java.awt.event.MouseEvent;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import com.google.common.collect.HashMultiset;
import com.google.common.collect.Multiset;
import com.google.common.collect.Multiset.Entry;
import com.google.common.collect.Multisets;
import com.mercutio.streams.twitch.api.Stream;

public class SettingsDialog extends JFrame implements DataChangedListener {

	private static final long serialVersionUID = 1L;
	private DefaultTableModel tableModel;
	
	private static final String TOOLTIP_TEMPLATE = "<html><center><h2>%s</h2><img src=http://static-cdn.jtvnw.net/ttv-boxart/%s-136x190.jpg /></center></html>";
	
	public SettingsDialog(final FavoriteManager favoriteManager) {
		super("StreamViewer Settings");

		Vector<String> columns = new Vector<>(Arrays.asList("", "Name", "Channels", "Viewers"));
		Vector<Vector<Object>> data = new Vector<>(Arrays.asList(new Vector<>(Arrays.asList(false, "", "Loading...", ""))));

		JTable table = FavoriteTableUtil.createFavoriteTable(this, new TableClickListener() {
			@Override
			public void tableClicked(TableModel tableModel, int row, int col, MouseEvent source) {
				if (col == 0) {
					boolean favorite = !Boolean.TRUE.equals(tableModel.getValueAt(row, 0));
					String game = (String) tableModel.getValueAt(row, 1);
					System.out.println((favorite ? "Add" : "Remove") + " Game: " + game);
					favoriteManager.setFavoriteGame(game, favorite);
					tableModel.setValueAt(favorite, row, 0);
				}
			}
		}, new TooltipProvider() {
			@Override
			public String getTooltip(int row) {
				try {
					Object name = tableModel.getValueAt(row, 1);
					return String.format(TOOLTIP_TEMPLATE, name, URLEncoder.encode((String) name, "UTF-8").replace("+", "%20"));
				} catch (UnsupportedEncodingException e) {
					throw new RuntimeException(e);
				}
			}
		}, data, columns, ImageProducer.getImage(ImageProducer.IMG_FAVORITE), ImageProducer.getImage(ImageProducer.IMG_NON_FAVORITE));

		table.getColumnModel().getColumn(2).setMaxWidth(75);
		table.getColumnModel().getColumn(3).setMaxWidth(75);

		tableModel = (DefaultTableModel) table.getModel();

		setIconImages(ImageProducer.getImages(ImageProducer.IMG_TRAY_ICONS));
		pack();
		setLocationByPlatform(true);
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);

	}

	@Override
	public void updateData(DataProvider provider) {
		System.out.println("Updating Data (SettingsDialog)");
		Multiset<String> gamesC = HashMultiset.create();
		Multiset<String> channels = HashMultiset.create();
		for (Stream stream : provider.getStreams()) {
			String game = stream.getGame();
			gamesC.add(game, stream.getViewers());
			channels.add(game);
		}

		Set<String> remaining = new HashSet<>(provider.getFavoriteGames());

		final Multiset<String> games = Multisets.copyHighestCountFirst(gamesC);

		@SuppressWarnings("unchecked")
		Vector<Object> data = tableModel.getDataVector();
		data.clear();

		for (Entry<String> i : games.entrySet()) {
			String game = i.getElement();
			int chans = channels.count(game);
			int count = i.getCount();
			data.add(new Vector<>(Arrays.asList(remaining.remove(game), game, chans, count)));
		}
		// favorites that have no streams online in the top 100
		for (String game : remaining) {
			data.add(new Vector<>(Arrays.asList(true, game, 0, 0)));
		}

		tableModel.fireTableDataChanged();
	}
}
