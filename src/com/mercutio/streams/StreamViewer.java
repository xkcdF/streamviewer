package com.mercutio.streams;

import java.awt.DisplayMode;
import java.awt.Frame;
import java.awt.GraphicsConfiguration;
import java.awt.Insets;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.Window;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Vector;

import javax.swing.JDialog;
import javax.swing.JTable;
import javax.swing.RowSorter.SortKey;
import javax.swing.SortOrder;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import com.mercutio.streams.browsers.Browser;
import com.mercutio.streams.twitch.api.Stream;

public class StreamViewer extends JDialog implements DataChangedListener {

	private static final String DIALOG_FILE = "dialog.ser";

	private static final long serialVersionUID = 1L;

	private static final int DEFAULT_WIDTH = 400;
	private static final int DEFAULT_HEIGHT = 400;

	private List<Stream> streams = Collections.emptyList();

	private final DefaultTableModel tableModel;
	private final TableRowSorter<DefaultTableModel> rowSorter;

	public StreamViewer(final FavoriteManager favoriteManager) {
		super((Frame) null, "StreamViewer", false);

		Vector<Vector<Object>> data = new Vector<>(Arrays.asList(new Vector<>(Arrays.asList(false, "", "Loading...", ""))));
		Vector<String> columns = new Vector<>(Arrays.asList("", "Game", "Stream", "Viewers"));

		JTable table = FavoriteTableUtil.createFavoriteTable(this, new TableClickListener() {
			@Override
			public void tableClicked(TableModel tableModel, int row, int col, MouseEvent source) {
				if (streams.isEmpty())
					return;

				row = rowSorter.convertRowIndexToModel(row);

				Stream stream = streams.get(row);
				if (col == 0) {
					System.out.println("Favorite: " + stream.getChannel().getName());
					boolean favorite = !Boolean.TRUE.equals(tableModel.getValueAt(row, 0));
					if (favoriteManager != null)
						favoriteManager.setFavoriteStream(stream.getChannel().get_id(), favorite);
					tableModel.setValueAt(favorite, row, 0);
				} else if (col > 0) {
					Browser favorite = favoriteManager.getFavoriteBrowser();
					if((source.getModifiersEx() & InputEvent.SHIFT_DOWN_MASK) != 0) {
						System.out.println("Open Stream (Comments): " + stream.getChannel().getDisplay_name());
						if(favorite == Browser.LIVESTREAMER) favorite = Browser.CHROME;
						favorite.open(getCommentsUrl(stream));
					} else {
						System.out.println("Open Stream: " + stream.getChannel().getDisplay_name());
						favorite.open(stream.getChannel().getUrl());
					}
				}
			}

			public URL getCommentsUrl(Stream stream) {
				try {
					return new URL(String.format("http://www.twitch.tv/chat/embed?channel=%s", stream.getChannel().getName()));
				} catch (MalformedURLException e) {
					throw new RuntimeException(e);
				}
			}
		}, new TooltipProvider() {
			@Override
			public String getTooltip(int row) {
				if (streams.isEmpty())
					return null;

				row = rowSorter.convertRowIndexToModel(row);
				return streams.get(row).getChannel().getStatus();
			}
		}, data, columns, ImageProducer.getImage(ImageProducer.IMG_FAVORITE), ImageProducer.getImage(ImageProducer.IMG_NON_FAVORITE));

		table.getColumnModel().getColumn(3).setMaxWidth(75);

		tableModel = (DefaultTableModel) table.getModel();
		rowSorter = new TableRowSorter<DefaultTableModel>(tableModel) {
			@Override
			public void toggleSortOrder(int column) {
				if (isSortable(column)) {
					List<SortKey> keys = new ArrayList<SortKey>(getSortKeys());
					int sortIndex;
					for (sortIndex = keys.size() - 1; sortIndex >= 0; sortIndex--) {
						if (keys.get(sortIndex).getColumn() == column) {
							break;
						}
					}
					if (sortIndex == -1) {
						// Key doesn't exist
						addNewSort(column, keys);
					} else if (sortIndex == 0) {
						// It's the primary sorting key, toggle it
						keys.set(0, toggle(keys.get(0)));
					} else {
						// It's not the first, but was sorted on, remove old
						// entry, insert as first with default sort.
						keys.remove(sortIndex);
						addNewSort(column, keys);
					}
					if (keys.size() > getMaxSortKeys()) {
						keys = keys.subList(0, getMaxSortKeys());
					}
					setSortKeys(keys);
				}
			}

			private void addNewSort(int column, List<SortKey> keys) {
				keys.add(0, new SortKey(column, getDefaultSortOrder(column)));
			}

			private SortOrder getDefaultSortOrder(int column) {
				return column == 0 || column == 3 ? SortOrder.DESCENDING : SortOrder.ASCENDING;
			}

			private SortKey toggle(SortKey key) {
				if (key.getSortOrder() == SortOrder.ASCENDING) {
					return new SortKey(key.getColumn(), SortOrder.DESCENDING);
				}
				return new SortKey(key.getColumn(), SortOrder.ASCENDING);
			}
		};

		rowSorter.setSortsOnUpdates(true);
		rowSorter.setMaxSortKeys(5);
		table.setRowSorter(rowSorter);

		setIconImages(ImageProducer.getImages(ImageProducer.IMG_TRAY_ICONS));

		load();

		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
	}

	@Override
	@SuppressWarnings("unchecked")
	public void updateData(DataProvider provider) {
		streams = new ArrayList<>();

		Vector<Object> data = tableModel.getDataVector();
		data.clear();

		for (Stream s : provider.getStreams()) {
			boolean favorite = provider.getFavoriteStreams().contains(s.getChannel().get_id());
			if (favorite || provider.getFavoriteGames().contains(s.getGame())) {
				streams.add(s);
				data.add(new Vector<>(Arrays.asList(favorite, s.getGame(), s.getChannel().getDisplay_name(), s.getViewers())));
			}
		}

		tableModel.fireTableDataChanged();
	}

	public void save() {
		try (ObjectOutputStream oos = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(DIALOG_FILE)))) {
			oos.writeInt(getWidth());
			oos.writeInt(getHeight());

			Point location = getLocation();
			oos.writeInt(location.x);
			oos.writeInt(location.y);

			List<? extends SortKey> sortKeys = rowSorter.getSortKeys();

			oos.writeInt(sortKeys.size());
			for (SortKey key : sortKeys) {
				oos.writeInt(key.getColumn());
				oos.writeBoolean(key.getSortOrder() == SortOrder.DESCENDING);
			}

			System.out.println("Saved " + sortKeys.size() + " to " + DIALOG_FILE);
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}

	public void load() {
		try (ObjectInputStream ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream(DIALOG_FILE)))) {
			setSize(ois.readInt(), ois.readInt());
			setLocation(ois.readInt(), ois.readInt());

			int length = ois.readInt();
			List<SortKey> keys = new ArrayList<>(length);
			for (int i = 0; i < length; i++) {
				keys.add(new SortKey(ois.readInt(), ois.readBoolean() ? SortOrder.DESCENDING : SortOrder.ASCENDING));
			}
			rowSorter.setSortKeys(keys);

			System.out.println("Read "+ length + " from " + DIALOG_FILE);
			return;
		} catch (FileNotFoundException fnf) {
			System.err.println("No Dialog settings found");
		} catch (IOException e) {
			e.printStackTrace();
		}

		setWindowLocation(this, DEFAULT_WIDTH, DEFAULT_HEIGHT, true, true);

		rowSorter.setSortKeys(Arrays.asList(new SortKey(3, SortOrder.DESCENDING)));

	}

	public static void setWindowLocation(Window target, int width, int height, boolean right, boolean bottom) {
		target.setSize(width, height);
		GraphicsConfiguration gc = target.getGraphicsConfiguration();
		DisplayMode dm = gc.getDevice().getDisplayMode();
		Insets screeninsets = Toolkit.getDefaultToolkit().getScreenInsets(gc);
		target.setLocation(
				right ? dm.getWidth() - screeninsets.right - width : screeninsets.left,
				bottom ? dm.getHeight() - screeninsets.bottom - height : screeninsets.top);
	}
	
	@Override
	public void dispose() {
		if(isVisible() && isDisplayable())
			save();
		super.dispose();
	}
}
