package com.mercutio.streams;

import java.awt.event.MouseEvent;

import javax.swing.table.TableModel;

public interface TableClickListener {
	void tableClicked(TableModel tableModel, int row, int col, MouseEvent source);
}
