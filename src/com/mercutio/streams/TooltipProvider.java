package com.mercutio.streams;

public interface TooltipProvider {
	String getTooltip(int row);
}
