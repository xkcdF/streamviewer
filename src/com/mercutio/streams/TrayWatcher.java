package com.mercutio.streams;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.PopupMenu;
import java.awt.RenderingHints;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Line2D;
import java.awt.image.BufferedImage;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import javax.swing.SwingUtilities;
import javax.swing.event.EventListenerList;

import com.google.common.collect.Sets;
import com.mercutio.streams.browsers.Browser;
import com.mercutio.streams.db.DatabaseManager;
import com.mercutio.streams.twitch.api.Stream;

public class TrayWatcher extends TrayIcon implements FavoriteManager, DataProvider {
	protected EventListenerList listenerList = new EventListenerList();

	private static final Color NEW_FAVORITES = new Color(255, 131, 80);
	private static final String FAVORITES_FILE = "favorites.ser";

	private final Image baseIcon;

	private Set<Integer> favorites = new HashSet<>();
	private Set<Integer> favoritesOnline = new HashSet<>();
	private Set<Integer> unreadFavoritesOnline = new HashSet<>();

	private Set<String> favoriteGames = new HashSet<>();
	private Collection<Stream> streams = Collections.emptyList();

	private Browser favoriteBrowser;

	private Object currentIconType;

	private ActionListener combinedListener;
	public DatabaseManager databaseManager;

	public TrayWatcher() {
		super(ImageProducer.getImage(ImageProducer.IMG_TRAY_ICON).getImage(), "Initializing...", new PopupMenu());

		loadFavorites();
		baseIcon = getImage();

		addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent ev) {
				if (!SwingUtilities.isRightMouseButton(ev) && combinedListener != null) {
					ActionEvent e = new ActionEvent(TrayWatcher.this, ActionEvent.ACTION_FIRST, getActionCommand());
					fireCombinedActionEvent(e);
				}
			}
		});
		addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				fireCombinedActionEvent(e);
			}
		});
	}

	@SuppressWarnings("unchecked")
	public void loadFavorites() {
		try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(FAVORITES_FILE))) {
			favorites = (Set<Integer>) ois.readObject();
			favoriteGames = (Set<String>) ois.readObject();
			favoriteBrowser = Browser.values()[ois.readInt()];
		} catch (ClassNotFoundException | IOException | ArrayIndexOutOfBoundsException e) {
			e.printStackTrace();
			if (favoriteGames == null)
				favoriteGames = Sets.newHashSet(
					"Dota 2",
					"StarCraft II: Heart of the Swarm",
					"StarCraft II: Wings of Liberty",
					"Minecraft",
					"DayZ");
			favoriteBrowser = Browser.DEFAULT;
		}
	}

	public void saveFavorites() {
		try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(FAVORITES_FILE))) {
			oos.writeObject(favorites);
			oos.writeObject(favoriteGames);
			oos.writeInt(favoriteBrowser.ordinal());
		} catch (IOException ex) {
			ex.printStackTrace();
		}
		try {
			getDatabaseManager().save(favorites, favoriteGames);
		} catch(Exception ex) {
			ex.printStackTrace();
		}
	}

	@Override
	public Collection<Stream> getStreams() {
		return streams;
	}

	/**
	 * @see #fireDataChanged()
	 */
	public void setStreams(Collection<Stream> streams, boolean dialogActive) {
		this.streams = streams;

		Set<Integer> newFavsOnline = new HashSet<>();

		for (Stream stream : streams) {
			int id = stream.getChannel().get_id();

			if (favorites.contains(id)) {
				newFavsOnline.add(id);
			}
		}

		// keep only favorites that are still online
		unreadFavoritesOnline.retainAll(newFavsOnline);

		// all favorites that were not already part of the favorites
		Set<Integer> additional = new HashSet<>(newFavsOnline);
		additional.removeAll(favoritesOnline);

		// notify if additional unread favorites have been added
		boolean updated = unreadFavoritesOnline.addAll(additional);

		favoritesOnline = newFavsOnline;

		updateTooltip(!dialogActive && updated);

		fireDataChanged();
	}

	private void updateTooltip(boolean notify) {
		int size = favoritesOnline.size();
		StringBuilder tooltip = new StringBuilder();
		tooltip.append(size).append(" favorite");
		tooltip.append(size != 1 ? "s" : "").append(" online");
		setToolTip(tooltip.toString());

		if (notify) {
			for (Stream stream : streams) {
				if (unreadFavoritesOnline.contains(stream.getChannel().get_id())) {
					int len = tooltip.length();
					tooltip.append("\n").append(stream.getChannel().getDisplay_name());
					tooltip.append(" (").append(stream.getGame()).append(") ");
					tooltip.append(abbreviate(stream.getChannel().getStatus(), 50 - tooltip.length() + len));
				}
			}

			displayMessage("New Favorite Streams online", tooltip.toString(), MessageType.INFO);
		}

		updateIcon();
	}

	private static String abbreviate(String str, int max) {
		if (max <= 3)
			return "...";
		if (str.length() > max)
			return str.substring(0, max - 3) + "...";
		return str;
	}

	private void updateIcon() {
		NumberIconType type = new NumberIconType(favoritesOnline.size(), !unreadFavoritesOnline.isEmpty());
		if (!type.equals(currentIconType)) {
			System.out.println("New icon");
			currentIconType = type;
			setImage(getNumberIcon(type));
		}
	}

	@Override
	public void setFavoriteStream(Integer id, boolean favorite) {
		boolean save;
		if (favorite) {
			save = favorites.add(id);
			favoritesOnline.add(id);
		} else {
			save = favorites.remove(id);
			favoritesOnline.remove(id);
		}
		if (save)
			saveFavorites();
		updateTooltip(false);
	}

	@Override
	public Set<Integer> getFavoriteStreams() {
		return favorites;
	}

	private Image getNumberIcon(NumberIconType type) {
		if (type.favs == 0)
			return baseIcon;

		String msg = "" + type.favs;
		BufferedImage bim = new BufferedImage(baseIcon.getWidth(null), baseIcon.getHeight(null), BufferedImage.TYPE_INT_ARGB);
		Graphics2D g = bim.createGraphics();
		g.drawImage(baseIcon, 0, 0, null);
		g.setFont(new Font("Arial", 1, 12));
		int x = (int) (bim.getWidth() - 1 - g.getFontMetrics().getStringBounds(msg, g).getWidth());
		int y = bim.getHeight() - 1;

		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

		if (type.unupdated)
			drawCross(g);

		g.scale(0.95, 0.95);
		g.setColor(Color.BLACK);
		g.drawString(msg, x + 1, y + 1);
		g.setColor(type.unupdated ? NEW_FAVORITES : Color.WHITE);
		g.drawString(msg, x, y);
		g.dispose();

		return bim;
	}

	private void drawCross(Graphics2D g) {
		float x = .5f, y = 2, r = 4;
		float translate = 1f;
		Line2D.Float bar = new Line2D.Float(x + r / 2, y, x + r / 2, y + r);
		Line2D.Float line = new Line2D.Float(x, y + r / 2, x + r, y + r / 2);

		g.setStroke(new BasicStroke(1.5f));
		g.setColor(new Color(0f, 0f, 0f, 0.25f));
		g.translate(translate, translate);
		g.draw(bar);
		g.draw(line);
		g.translate(-translate, -translate);
		g.setColor(NEW_FAVORITES);
		g.draw(bar);
		g.draw(line);
	}

	public ActionListener getCombinedListener() {
		return combinedListener;
	}

	public void setCombinedListener(ActionListener combinedListener) {
		this.combinedListener = combinedListener;
	}

	private void fireCombinedActionEvent(ActionEvent e) {
		if (!unreadFavoritesOnline.isEmpty()) {
			unreadFavoritesOnline.clear();
			updateIcon();
		}
		combinedListener.actionPerformed(e);
	}

	@Override
	public Set<String> getFavoriteGames() {
		return favoriteGames;
	}

	@Override
	public void setFavoriteGame(String game, boolean favorite) {
		boolean save;
		if (favorite)
			save = favoriteGames.add(game);
		else
			save = favoriteGames.remove(game);
		if (save) {
			saveFavorites();
			fireDataChanged();
		}
	}

	public void addDataChangedListener(DataChangedListener l) {
		listenerList.add(DataChangedListener.class, l);
	}

	public void removeDataChangedListener(DataChangedListener l) {
		listenerList.remove(DataChangedListener.class, l);
	}

	public void fireDataChanged() {
		Object[] listeners = listenerList.getListenerList();
		for (int i = 0; i < listeners.length; i += 2) {
			if (listeners[i] == DataChangedListener.class) {
				((DataChangedListener) listeners[i + 1]).updateData(this);
			}
		}
	}

	@Override
	public Browser getFavoriteBrowser() {
		return favoriteBrowser;
	}

	@Override
	public void setFavoriteBrowser(Browser browser) {
		if(browser != favoriteBrowser) {
			favoriteBrowser = browser;
			saveFavorites();
		}
	}
	
	public DatabaseManager getDatabaseManager() throws ClassNotFoundException, SQLException, IOException {
		if (databaseManager == null) {
			databaseManager = new DatabaseManager();
		}

		return databaseManager;
	}

}
