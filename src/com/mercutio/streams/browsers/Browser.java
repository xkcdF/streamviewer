package com.mercutio.streams.browsers;

import java.awt.BorderLayout;
import java.awt.Desktop;
import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Vector;

import javax.swing.AbstractAction;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

import com.mercutio.streams.Quality;
import com.mercutio.streams.StreamViewer;

public enum Browser {
	DEFAULT, CHROME, FIREFOX, LIVESTREAMER;

	private final class QualityComparator implements Comparator<String> {
		@Override
		public int compare(String a, String b) {
			return Integer.compare(val(a), val(b));
		}

		private int val(String o) {
			return Quality.valueOf(o.toUpperCase()).ordinal();
		}
	}

	public void open(final URL target) {
		switch (this) {
		case DEFAULT:
			try {
				Desktop.getDesktop().browse(target.toURI());
			} catch (IOException | URISyntaxException ex) {
				ex.printStackTrace();
			}
			break;
		case CHROME:
			try {
				Runtime.getRuntime().exec("C:/Program Files (x86)/Google/Chrome/Application/chrome.exe " + target);
			} catch (IOException e) {
				e.printStackTrace();
			}
			break;
		case FIREFOX:
			try {
				Runtime.getRuntime().exec("C:/Program Files (x86)/Mozilla Firefox/firefox.exe " + target);
			} catch (IOException e) {
				e.printStackTrace();
			}
			break;
		case LIVESTREAMER:
			Thread livestreamer = new Thread(new Runnable() {
				private Process started;

				@Override
				public void run() {
					final JFrame display = new JFrame("Starting stream...");
					
					try {
						final String best = "best";
						started = startLivestreamer(target, best);
						final JTextArea output = new JTextArea("Initializing..\n");
						final Document doc = output.getDocument();
						final Vector<String> values = new Vector<>(Arrays.asList(best, "worst"));
						final DefaultComboBoxModel<String> qualities = new DefaultComboBoxModel<>(values);
						final JPanel tools = new JPanel();
						SwingUtilities.invokeLater(new Runnable() {
							@Override
							public void run() {
								display.add(new JScrollPane(output));
								tools.add(new JLabel("Quality: "));
								JComboBox<String> qual = new JComboBox<String>(qualities);
								// reserve enough space for the largest quality name
								qual.setPrototypeDisplayValue(Quality.MOBILE_MEDIUM.name().toLowerCase());
								tools.add(qual);
								tools.add(new JButton(new AbstractAction("Apply") {
									private static final long serialVersionUID = 1L;

									@Override
									public void actionPerformed(ActionEvent e) {
										started.destroy();
										try {
											started = startLivestreamer(target, (String) qualities.getSelectedItem());
											new Thread(new Runnable() {
												@Override
												public void run() {
													pipeToDoc(started.getInputStream(), doc);
												}
											}).start();
										} catch (IOException ex) {
											ex.printStackTrace();
										}
									}
								}));
								display.add(tools, BorderLayout.SOUTH);
								display.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
								StreamViewer.setWindowLocation(display, 600, 300, false, true);
								display.setVisible(true);
							}
						});
						Thread availableStreams = new Thread(new Runnable() {
							@Override
							public void run() {
								try {
									String[] quals = pipeToDoc(startLivestreamer(target, "").getInputStream(), null)
											.replace('\n', ' ').replace("Available streams:", "")
											.replace("(worst)", "").replace("(best)", "")
											.trim().split("\\s*,\\s*");
									for(String s : quals) {
										int i = Collections.binarySearch(values, s, new QualityComparator());
										if(i < 0) {
											qualities.insertElementAt(s, -i-1);
										}
									}
									
									checkSort(values);
									System.out.println("Qualities " + values);
								} catch (IOException e) {
									e.printStackTrace();
								}
							}

							private void checkSort(List<String> values) {
								List<String> check = new ArrayList<>(values);
								Collections.sort(check, new QualityComparator());
								if(!check.equals(values))
									System.out.println(check + " / " + values);
							}
							
						}, "Get Available Streams");
						availableStreams.setDaemon(true);
						availableStreams.start();
						pipeToDoc(started.getInputStream(), doc);
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

				private Process startLivestreamer(final URL target, String quality) throws IOException {
					return new ProcessBuilder("livestreamer", target.toString(), quality).redirectErrorStream(true).start();
				}
			}, "Livestreamer " + target);
			livestreamer.setDaemon(true);
			livestreamer.start();
			break;
		default:
			break;
		}
	}

	@Override
	public String toString() {
		return name().substring(0, 1) + name().substring(1).toLowerCase();
	}

	/**
	 * Pipes output of Process to the document
	 */
	public String pipeToDoc(InputStream input, final Document doc) {
		String last = "";
		try (final BufferedReader stream = new BufferedReader(new InputStreamReader(input))) {
			for (String line = null;(line = stream.readLine()) != null;) {
				last = line;
				System.out.println(line);
				if(doc == null) continue;
				final String transport = line + "\n";
				try {
					doc.insertString(doc.getLength(), transport, null);
				} catch (BadLocationException e) {
					e.printStackTrace();
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return last;
	}
}
