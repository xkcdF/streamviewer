package com.mercutio.streams.charts;

import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.swing.JFrame;
import javax.swing.SwingUtilities;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.block.BlockBorder;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYDotRenderer;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;
import org.jfree.ui.HorizontalAlignment;

import com.mercutio.streams.db.DatabaseManager;
import com.mercutio.streams.db.generated.tables.records.ByfavoritegameRecord;
import com.mercutio.streams.db.generated.tables.records.ByfavoritestreamerRecord;
import com.mercutio.streams.db.generated.tables.records.GamesRecord;
import com.mercutio.streams.db.generated.tables.records.StreamersRecord;

public class History extends JFrame {
	private static final long serialVersionUID = 1L;
	
	
	public History(XYDataset dataset) {
		super(BY_GAME ? "Game History" : "Viewer History");
		
		add(new ChartPanel(createChart(dataset)));
		
		setDefaultCloseOperation(DISPOSE_ON_CLOSE);
		pack();
		setLocationRelativeTo(null);
		setVisible(true);
	}


	private static JFreeChart createChart(XYDataset xydataset) {
		JFreeChart jfreechart = ChartFactory.createTimeSeriesChart(BY_GAME ? "Favorite Games" : "Favorite Streamers",
				null, "Viewers", xydataset, true, true, false);
		XYPlot xyplot = (XYPlot) jfreechart.getPlot();
		xyplot.setDomainPannable(true);
		xyplot.setDomainCrosshairVisible(true);
		xyplot.setRangeCrosshairVisible(true);
		xyplot.getDomainAxis().setLowerMargin(0.0D);
		jfreechart.getLegend().setFrame(BlockBorder.NONE);
		jfreechart.getLegend().setHorizontalAlignment(HorizontalAlignment.CENTER);
		XYDotRenderer renderer = new XYDotRenderer();
		renderer.setDotHeight(4);
		renderer.setDotWidth(4);
		xyplot.setRenderer(renderer);
		return jfreechart;
	}
	
	private static final boolean BY_GAME = true;

	public static void main(String[] args) throws ClassNotFoundException, SQLException, IOException {
		final TimeSeriesCollection dataset = new TimeSeriesCollection();
		
		Map<Integer, TimeSeries> sets = new HashMap<>();
		
		try(DatabaseManager dm = new DatabaseManager()) {
			if(BY_GAME)
				populateByGame(sets, dm);
			else
				populateByStreamer(sets, dm);
		}
		
		for(TimeSeries s : sets.values()) {
			dataset.addSeries(s);
		}
		
		
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				new History(dataset);
			}
		});
	}


	private static void populateByStreamer(Map<Integer, TimeSeries> sets, DatabaseManager dm) {
		for(StreamersRecord streamer : dm.getFavoriteStreamers()) {
			sets.put(streamer.getId(), new TimeSeries(streamer.getName()));
		}
		
		for(ByfavoritestreamerRecord record : dm.getByFavoriteStreamer()) {
			Integer streamer = record.getStreamer();
			Integer viewers = record.getViewers();
			try {
				Date date = parseSeconds(record.getD());
				sets.get(streamer).add(new Second(date), viewers);;
			} catch(Exception e) {
				System.err.println(streamer + " / " + record.getD() + " / " + viewers);
				e.printStackTrace();
			}
		}
	}
	
	private static void populateByGame(Map<Integer, TimeSeries> sets, DatabaseManager dm) {
		for(GamesRecord game : dm.getFavoriteGames()) {
			sets.put(game.getId(), new TimeSeries(game.getName()));
		}
		
		for(ByfavoritegameRecord record : dm.getByFavoriteGame()) {
			Integer game = record.getGame();
			Number viewers = (Number) record.getTotal();
			try {
				Date date = parseSeconds(record.getD());
				sets.get(game).add(new Second(date), viewers);;
			} catch(Exception e) {
				System.err.println(game + " / " + record.getD() + " / " + viewers);
				e.printStackTrace();
			}
		}
	}


	private static Date parseSeconds(Object seconds) {
		return new Date(1000 * (Integer)  seconds);
	}
}
