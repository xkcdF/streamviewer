package com.mercutio.streams.db;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import com.google.common.base.Charsets;
import com.google.common.io.Resources;

public class BuildDatabase {
	
	public static Connection createDatabase() throws SQLException, IOException {
		Connection conn = DriverManager.getConnection("jdbc:sqlite:history.db");
		try(Statement s = conn.createStatement()) {
    		String buildScript = Resources.toString(Resources.getResource("database.sql"), Charsets.UTF_8);
			for(String statement : buildScript.replaceAll("CREATE [A-Z\\s]+(?=\\[)", "$0 IF NOT EXISTS ").split("\\s*;\\s*")) {
    			s.execute(statement);
    		}
    	}
		return conn;
	}
	
    public static void main(String... args) throws ClassNotFoundException, SQLException, IOException {
        Class.forName("org.sqlite.JDBC");
        createDatabase().close();
    }
  }