package com.mercutio.streams.db;

import static org.jooq.impl.DSL.*;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.jooq.Configuration;
import org.jooq.Result;
import org.jooq.SQLDialect;
import org.jooq.conf.Settings;
import org.jooq.exception.DataAccessException;
import org.jooq.impl.DefaultConfiguration;
import org.jooq.impl.DefaultConnectionProvider;

import com.mercutio.streams.db.generated.tables.Byfavoritegame;
import com.mercutio.streams.db.generated.tables.Byfavoritestreamer;
import com.mercutio.streams.db.generated.tables.Entries;
import com.mercutio.streams.db.generated.tables.Favoritegames;
import com.mercutio.streams.db.generated.tables.Favoritestreamers;
import com.mercutio.streams.db.generated.tables.Games;
import com.mercutio.streams.db.generated.tables.Streamers;
import com.mercutio.streams.db.generated.tables.records.ByfavoritegameRecord;
import com.mercutio.streams.db.generated.tables.records.ByfavoritestreamerRecord;
import com.mercutio.streams.db.generated.tables.records.GamesRecord;
import com.mercutio.streams.db.generated.tables.records.StreamersRecord;
import com.mercutio.streams.twitch.api.Stream;

public class DatabaseManager implements AutoCloseable {
	private static final boolean DEBUG = false;
	

	private final Connection con;
	private final Configuration conf;
	

	public DatabaseManager() throws SQLException, ClassNotFoundException, IOException {
		Class.forName("org.sqlite.JDBC");
		con = BuildDatabase.createDatabase();
		con.setAutoCommit(false);
		conf = new DefaultConfiguration().set(SQLDialect.SQLITE).set(new DefaultConnectionProvider(con)).set(new Settings().withExecuteLogging(DEBUG));
	}

	@Override
	public void close() throws SQLException {
		con.close();
	}
	
	private final Map<String, Integer> gameCache = new HashMap<>();

	public Integer gameRecord(String name) {
		Integer id = gameCache.get(name);
		if(id == null) {
			Games g = Games.GAMES;
			try {
				using(conf).insertInto(g).set(g.NAME, name).execute();
				id = using(conf).lastID().intValue();
			} catch(DataAccessException e) {
				Throwable cause = e.getCause();
				if(cause instanceof SQLException && cause.getMessage().startsWith("[SQLITE_CONSTRAINT]")) {
					id = using(conf).select(g.ID).from(g).where(g.NAME.eq(name)).fetchOne().value1();
				} else {
					throw e;
				}
			}
			gameCache.put(name, id);
		}
		return id;
	}
	
	private Long now;
	
	public void updateNow(){
		if (now == null) {
			now = System.currentTimeMillis() / 1000;
		}
	}
	
	public void done() {
		now = null;
	}
	
	public void save(List<Stream> streams) throws SQLException {
		long start = System.currentTimeMillis();
		
		try {
			updateNow();
			System.out.println(now);
			
			// update all channel names
			PreparedStatement streamers = con.prepareStatement("REPLACE INTO streamers (id, name) VALUES (?, ?)");
			PreparedStatement entries = con.prepareStatement("INSERT INTO entries (date, streamer, game, viewers) VALUES (?, ?, ?, ?)");
			
			for(Stream stream : streams) {
				streamers.setObject(1, stream.getChannel().get_id());
				streamers.setString(2, stream.getChannel().getDisplay_name());
				streamers.addBatch();
				
				entries.setObject(1, now);
				entries.setObject(2, stream.getChannel().get_id());
				entries.setObject(3, gameRecord(stream.getGame()));
				entries.setObject(4, stream.getViewers());
				entries.addBatch();
			}
			
			streamers.executeBatch();
			entries.executeBatch();
			con.commit();
		} catch (SQLException | RuntimeException e) {
			con.rollback();
			throw e;
		}

		System.out.printf("Saved %d streams in %.3f seconds.%n", streams.size(), (System.currentTimeMillis() - start) * 1e-3);
	}
	
	public void save(Set<Integer> streams, Set<String> games) throws SQLException{
		try {
			Statement stmt = con.createStatement();
			stmt.execute("DELETE FROM favoriteGames");
			stmt.execute("DELETE FROM favoriteStreamers");

			PreparedStatement insert = con.prepareStatement("INSERT INTO favoriteStreamers (id) VALUES (?)");
			for(Integer stream : streams) {
				insert.setInt(1, stream);
				insert.addBatch();
			}
			insert.executeBatch();
			
			insert = con.prepareStatement("INSERT INTO favoriteGames (id) VALUES (?)");
			for(String game : games) {
				insert.setInt(1, gameRecord(game));
				insert.addBatch();
			}
			insert.executeBatch();
			
			con.commit();
		} catch(SQLException | RuntimeException e) {
			con.rollback();
			throw e;
		}
		
	}

	/**
	 * Gets the favorite streamers which have items in the entries table
	 */
	public Result<StreamersRecord> getFavoriteStreamers() {
		Streamers streamers = Streamers.STREAMERS;
		Entries entries = Entries.ENTRIES;
		Favoritestreamers fS = Favoritestreamers.FAVORITESTREAMERS;
		return using(conf)
				.selectFrom(streamers)
				.where(streamers.ID.in(
					selectDistinct(fS.ID)
					.from(fS)
					.join(entries)
					.on(fS.ID.eq(entries.STREAMER)))).fetch();
	}

	public Result<ByfavoritestreamerRecord> getByFavoriteStreamer() {
		return using(conf).selectFrom(Byfavoritestreamer.BYFAVORITESTREAMER).fetch();
	}

	public Result<GamesRecord> getFavoriteGames() {
		Games games = Games.GAMES;
		Entries entries = Entries.ENTRIES;
		Favoritegames fS = Favoritegames.FAVORITEGAMES;
		return using(conf)
				.selectFrom(games)
				.where(games.ID.in(
					selectDistinct(fS.ID)
					.from(fS)
					.join(entries)
					.on(fS.ID.eq(entries.GAME)))).fetch();
	}

	public Result<ByfavoritegameRecord> getByFavoriteGame() {
		return using(conf).selectFrom(Byfavoritegame.BYFAVORITEGAME).fetch();
	}

}
