/**
 * This class is generated by jOOQ
 */
package com.mercutio.streams.db.generated;

/**
 * This class is generated by jOOQ.
 *
 * A class modelling foreign key relationships between tables of the <code></code> 
 * schema
 */
@javax.annotation.Generated(value    = { "http://www.jooq.org", "3.2.3" },
                            comments = "This class is generated by jOOQ")
@java.lang.SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Keys {

	// -------------------------------------------------------------------------
	// IDENTITY definitions
	// -------------------------------------------------------------------------


	// -------------------------------------------------------------------------
	// UNIQUE and PRIMARY KEY definitions
	// -------------------------------------------------------------------------

	public static final org.jooq.UniqueKey<com.mercutio.streams.db.generated.tables.records.EntriesRecord> PK_ENTRIES = UniqueKeys0.PK_ENTRIES;
	public static final org.jooq.UniqueKey<com.mercutio.streams.db.generated.tables.records.FavoritegamesRecord> PK_FAVORITEGAMES = UniqueKeys0.PK_FAVORITEGAMES;
	public static final org.jooq.UniqueKey<com.mercutio.streams.db.generated.tables.records.FavoritestreamersRecord> PK_FAVORITESTREAMERS = UniqueKeys0.PK_FAVORITESTREAMERS;
	public static final org.jooq.UniqueKey<com.mercutio.streams.db.generated.tables.records.GamesRecord> PK_GAMES = UniqueKeys0.PK_GAMES;
	public static final org.jooq.UniqueKey<com.mercutio.streams.db.generated.tables.records.StreamersRecord> PK_STREAMERS = UniqueKeys0.PK_STREAMERS;

	// -------------------------------------------------------------------------
	// FOREIGN KEY definitions
	// -------------------------------------------------------------------------

	public static final org.jooq.ForeignKey<com.mercutio.streams.db.generated.tables.records.EntriesRecord, com.mercutio.streams.db.generated.tables.records.StreamersRecord> FK_ENTRIES_STREAMERS_1 = ForeignKeys0.FK_ENTRIES_STREAMERS_1;
	public static final org.jooq.ForeignKey<com.mercutio.streams.db.generated.tables.records.EntriesRecord, com.mercutio.streams.db.generated.tables.records.GamesRecord> FK_ENTRIES_GAMES_1 = ForeignKeys0.FK_ENTRIES_GAMES_1;
	public static final org.jooq.ForeignKey<com.mercutio.streams.db.generated.tables.records.FavoritegamesRecord, com.mercutio.streams.db.generated.tables.records.GamesRecord> FK_FAVORITEGAMES_GAMES_1 = ForeignKeys0.FK_FAVORITEGAMES_GAMES_1;
	public static final org.jooq.ForeignKey<com.mercutio.streams.db.generated.tables.records.FavoritestreamersRecord, com.mercutio.streams.db.generated.tables.records.StreamersRecord> FK_FAVORITESTREAMERS_STREAMERS_1 = ForeignKeys0.FK_FAVORITESTREAMERS_STREAMERS_1;

	// -------------------------------------------------------------------------
	// [#1459] distribute members to avoid static initialisers > 64kb
	// -------------------------------------------------------------------------

	private static class UniqueKeys0 extends org.jooq.impl.AbstractKeys {
		public static final org.jooq.UniqueKey<com.mercutio.streams.db.generated.tables.records.EntriesRecord> PK_ENTRIES = createUniqueKey(com.mercutio.streams.db.generated.tables.Entries.ENTRIES, com.mercutio.streams.db.generated.tables.Entries.ENTRIES.ID);
		public static final org.jooq.UniqueKey<com.mercutio.streams.db.generated.tables.records.FavoritegamesRecord> PK_FAVORITEGAMES = createUniqueKey(com.mercutio.streams.db.generated.tables.Favoritegames.FAVORITEGAMES, com.mercutio.streams.db.generated.tables.Favoritegames.FAVORITEGAMES.ID);
		public static final org.jooq.UniqueKey<com.mercutio.streams.db.generated.tables.records.FavoritestreamersRecord> PK_FAVORITESTREAMERS = createUniqueKey(com.mercutio.streams.db.generated.tables.Favoritestreamers.FAVORITESTREAMERS, com.mercutio.streams.db.generated.tables.Favoritestreamers.FAVORITESTREAMERS.ID);
		public static final org.jooq.UniqueKey<com.mercutio.streams.db.generated.tables.records.GamesRecord> PK_GAMES = createUniqueKey(com.mercutio.streams.db.generated.tables.Games.GAMES, com.mercutio.streams.db.generated.tables.Games.GAMES.ID);
		public static final org.jooq.UniqueKey<com.mercutio.streams.db.generated.tables.records.StreamersRecord> PK_STREAMERS = createUniqueKey(com.mercutio.streams.db.generated.tables.Streamers.STREAMERS, com.mercutio.streams.db.generated.tables.Streamers.STREAMERS.ID);
	}

	private static class ForeignKeys0 extends org.jooq.impl.AbstractKeys {
		public static final org.jooq.ForeignKey<com.mercutio.streams.db.generated.tables.records.EntriesRecord, com.mercutio.streams.db.generated.tables.records.StreamersRecord> FK_ENTRIES_STREAMERS_1 = createForeignKey(com.mercutio.streams.db.generated.Keys.PK_STREAMERS, com.mercutio.streams.db.generated.tables.Entries.ENTRIES, com.mercutio.streams.db.generated.tables.Entries.ENTRIES.STREAMER);
		public static final org.jooq.ForeignKey<com.mercutio.streams.db.generated.tables.records.EntriesRecord, com.mercutio.streams.db.generated.tables.records.GamesRecord> FK_ENTRIES_GAMES_1 = createForeignKey(com.mercutio.streams.db.generated.Keys.PK_GAMES, com.mercutio.streams.db.generated.tables.Entries.ENTRIES, com.mercutio.streams.db.generated.tables.Entries.ENTRIES.GAME);
		public static final org.jooq.ForeignKey<com.mercutio.streams.db.generated.tables.records.FavoritegamesRecord, com.mercutio.streams.db.generated.tables.records.GamesRecord> FK_FAVORITEGAMES_GAMES_1 = createForeignKey(com.mercutio.streams.db.generated.Keys.PK_GAMES, com.mercutio.streams.db.generated.tables.Favoritegames.FAVORITEGAMES, com.mercutio.streams.db.generated.tables.Favoritegames.FAVORITEGAMES.ID);
		public static final org.jooq.ForeignKey<com.mercutio.streams.db.generated.tables.records.FavoritestreamersRecord, com.mercutio.streams.db.generated.tables.records.StreamersRecord> FK_FAVORITESTREAMERS_STREAMERS_1 = createForeignKey(com.mercutio.streams.db.generated.Keys.PK_STREAMERS, com.mercutio.streams.db.generated.tables.Favoritestreamers.FAVORITESTREAMERS, com.mercutio.streams.db.generated.tables.Favoritestreamers.FAVORITESTREAMERS.ID);
	}
}
