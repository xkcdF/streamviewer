/**
 * This class is generated by jOOQ
 */
package com.mercutio.streams.db.generated.tables;

/**
 * This class is generated by jOOQ.
 */
@javax.annotation.Generated(value    = { "http://www.jooq.org", "3.2.3" },
                            comments = "This class is generated by jOOQ")
@java.lang.SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class Favoritestreamers extends org.jooq.impl.TableImpl<com.mercutio.streams.db.generated.tables.records.FavoritestreamersRecord> {

	private static final long serialVersionUID = -1762345419;

	/**
	 * The singleton instance of <code>favoriteStreamers</code>
	 */
	public static final com.mercutio.streams.db.generated.tables.Favoritestreamers FAVORITESTREAMERS = new com.mercutio.streams.db.generated.tables.Favoritestreamers();

	/**
	 * The class holding records for this type
	 */
	@Override
	public java.lang.Class<com.mercutio.streams.db.generated.tables.records.FavoritestreamersRecord> getRecordType() {
		return com.mercutio.streams.db.generated.tables.records.FavoritestreamersRecord.class;
	}

	/**
	 * The column <code>favoriteStreamers.id</code>.
	 */
	public final org.jooq.TableField<com.mercutio.streams.db.generated.tables.records.FavoritestreamersRecord, java.lang.Integer> ID = createField("id", org.jooq.impl.SQLDataType.INTEGER.nullable(false), this);

	/**
	 * Create a <code>favoriteStreamers</code> table reference
	 */
	public Favoritestreamers() {
		super("favoriteStreamers", com.mercutio.streams.db.generated.DefaultSchema.DEFAULT_SCHEMA);
	}

	/**
	 * Create an aliased <code>favoriteStreamers</code> table reference
	 */
	public Favoritestreamers(java.lang.String alias) {
		super(alias, com.mercutio.streams.db.generated.DefaultSchema.DEFAULT_SCHEMA, com.mercutio.streams.db.generated.tables.Favoritestreamers.FAVORITESTREAMERS);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public org.jooq.UniqueKey<com.mercutio.streams.db.generated.tables.records.FavoritestreamersRecord> getPrimaryKey() {
		return com.mercutio.streams.db.generated.Keys.PK_FAVORITESTREAMERS;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public java.util.List<org.jooq.UniqueKey<com.mercutio.streams.db.generated.tables.records.FavoritestreamersRecord>> getKeys() {
		return java.util.Arrays.<org.jooq.UniqueKey<com.mercutio.streams.db.generated.tables.records.FavoritestreamersRecord>>asList(com.mercutio.streams.db.generated.Keys.PK_FAVORITESTREAMERS);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public java.util.List<org.jooq.ForeignKey<com.mercutio.streams.db.generated.tables.records.FavoritestreamersRecord, ?>> getReferences() {
		return java.util.Arrays.<org.jooq.ForeignKey<com.mercutio.streams.db.generated.tables.records.FavoritestreamersRecord, ?>>asList(com.mercutio.streams.db.generated.Keys.FK_FAVORITESTREAMERS_STREAMERS_1);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public com.mercutio.streams.db.generated.tables.Favoritestreamers as(java.lang.String alias) {
		return new com.mercutio.streams.db.generated.tables.Favoritestreamers(alias);
	}
}
