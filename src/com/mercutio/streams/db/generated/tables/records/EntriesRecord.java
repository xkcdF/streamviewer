/**
 * This class is generated by jOOQ
 */
package com.mercutio.streams.db.generated.tables.records;

/**
 * This class is generated by jOOQ.
 */
@javax.annotation.Generated(value    = { "http://www.jooq.org", "3.2.3" },
                            comments = "This class is generated by jOOQ")
@java.lang.SuppressWarnings({ "all", "unchecked", "rawtypes" })
public class EntriesRecord extends org.jooq.impl.UpdatableRecordImpl<com.mercutio.streams.db.generated.tables.records.EntriesRecord> implements org.jooq.Record5<java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer> {

	private static final long serialVersionUID = -1065954694;

	/**
	 * Setter for <code>entries.id</code>.
	 */
	public void setId(java.lang.Integer value) {
		setValue(0, value);
	}

	/**
	 * Getter for <code>entries.id</code>.
	 */
	public java.lang.Integer getId() {
		return (java.lang.Integer) getValue(0);
	}

	/**
	 * Setter for <code>entries.date</code>.
	 */
	public void setDate(java.lang.Integer value) {
		setValue(1, value);
	}

	/**
	 * Getter for <code>entries.date</code>.
	 */
	public java.lang.Integer getDate() {
		return (java.lang.Integer) getValue(1);
	}

	/**
	 * Setter for <code>entries.streamer</code>.
	 */
	public void setStreamer(java.lang.Integer value) {
		setValue(2, value);
	}

	/**
	 * Getter for <code>entries.streamer</code>.
	 */
	public java.lang.Integer getStreamer() {
		return (java.lang.Integer) getValue(2);
	}

	/**
	 * Setter for <code>entries.game</code>.
	 */
	public void setGame(java.lang.Integer value) {
		setValue(3, value);
	}

	/**
	 * Getter for <code>entries.game</code>.
	 */
	public java.lang.Integer getGame() {
		return (java.lang.Integer) getValue(3);
	}

	/**
	 * Setter for <code>entries.viewers</code>.
	 */
	public void setViewers(java.lang.Integer value) {
		setValue(4, value);
	}

	/**
	 * Getter for <code>entries.viewers</code>.
	 */
	public java.lang.Integer getViewers() {
		return (java.lang.Integer) getValue(4);
	}

	// -------------------------------------------------------------------------
	// Primary key information
	// -------------------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 */
	@Override
	public org.jooq.Record1<java.lang.Integer> key() {
		return (org.jooq.Record1) super.key();
	}

	// -------------------------------------------------------------------------
	// Record5 type implementation
	// -------------------------------------------------------------------------

	/**
	 * {@inheritDoc}
	 */
	@Override
	public org.jooq.Row5<java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer> fieldsRow() {
		return (org.jooq.Row5) super.fieldsRow();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public org.jooq.Row5<java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer, java.lang.Integer> valuesRow() {
		return (org.jooq.Row5) super.valuesRow();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public org.jooq.Field<java.lang.Integer> field1() {
		return com.mercutio.streams.db.generated.tables.Entries.ENTRIES.ID;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public org.jooq.Field<java.lang.Integer> field2() {
		return com.mercutio.streams.db.generated.tables.Entries.ENTRIES.DATE;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public org.jooq.Field<java.lang.Integer> field3() {
		return com.mercutio.streams.db.generated.tables.Entries.ENTRIES.STREAMER;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public org.jooq.Field<java.lang.Integer> field4() {
		return com.mercutio.streams.db.generated.tables.Entries.ENTRIES.GAME;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public org.jooq.Field<java.lang.Integer> field5() {
		return com.mercutio.streams.db.generated.tables.Entries.ENTRIES.VIEWERS;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public java.lang.Integer value1() {
		return getId();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public java.lang.Integer value2() {
		return getDate();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public java.lang.Integer value3() {
		return getStreamer();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public java.lang.Integer value4() {
		return getGame();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public java.lang.Integer value5() {
		return getViewers();
	}

	// -------------------------------------------------------------------------
	// Constructors
	// -------------------------------------------------------------------------

	/**
	 * Create a detached EntriesRecord
	 */
	public EntriesRecord() {
		super(com.mercutio.streams.db.generated.tables.Entries.ENTRIES);
	}

	/**
	 * Create a detached, initialised EntriesRecord
	 */
	public EntriesRecord(java.lang.Integer id, java.lang.Integer date, java.lang.Integer streamer, java.lang.Integer game, java.lang.Integer viewers) {
		super(com.mercutio.streams.db.generated.tables.Entries.ENTRIES);

		setValue(0, id);
		setValue(1, date);
		setValue(2, streamer);
		setValue(3, game);
		setValue(4, viewers);
	}
}
