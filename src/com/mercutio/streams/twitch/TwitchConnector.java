package com.mercutio.streams.twitch;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

import com.google.common.base.Charsets;
import com.google.gson.Gson;
import com.mercutio.streams.twitch.api.Overview;
import com.mercutio.streams.twitch.api.Stream;

public class TwitchConnector {

	public static final String BASE = "https://api.twitch.tv/kraken/streams";

	private static final int MIN_VIEWERS = 50;

	private URL next;

	public TwitchConnector() throws MalformedURLException {
		next = new URL(BASE);
	}

	public boolean hasNext() {
		return next != null;
	}
	
	private int retries = 0;
	private final int MAX_RETRIES = 9;
	
	private double readKB, elapsedSecs;
	
	public List<Stream> loadPage() throws IOException, InterruptedException {
		if (!hasNext())
			throw new IllegalStateException("Can not load more pages");

		URL current = next;
		next = null;

		URLConnection connection = current.openConnection();
		connection.connect();
		double length = connection.getContentLengthLong() / 1024d;
		long start = System.nanoTime();
		try (BufferedReader apiReader
			= new BufferedReader(new InputStreamReader(connection.getInputStream(), Charsets.UTF_8))) {
			Overview overview = new Gson().fromJson(apiReader, Overview.class);
			logTime(current, length, (System.nanoTime() - start) / 1000000000d, overview);

			URL next = overview.get_links().get("next");
			if (!overview.getStreams().isEmpty() &&
				overview.getStreams().get(overview.getStreams().size() - 1).getViewers() > MIN_VIEWERS)
				this.next = next;
			
			// successful connections slowly reset the retry counter
			retries /= 2;

			return overview.getStreams();
		} catch(IOException io) {
			retries++;
			if(retries > MAX_RETRIES)
				throw io;
			
			long sleep = Math.round(100 * Math.pow(1.5, retries));
			System.err.printf("Connection problem (%s / %s retries, sleeping for %4dms): %s%n", retries, MAX_RETRIES, sleep, io);
			Thread.sleep(sleep);
			next = current;
			return loadPage();
		}
	}

	private void logTime(URL current, double length, double seconds, Overview overview) {
		readKB += length;
		elapsedSecs += seconds;
		System.out.printf("Got %d streams from %s (%.1f KB in %.3f seconds @ %.2f KB/s)\n", overview.getStreams().size(),
				current, length, seconds,
				length / seconds);
	}

	public void logTotalTime() {
		double length = readKB;
		double seconds = elapsedSecs;
		System.out.printf("Finished reading Streams (%.1f KB in %.3f seconds @ %.2f KB/s)\n",
				length, seconds, length / seconds);
	}

}
