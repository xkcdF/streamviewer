package com.mercutio.streams.twitch.api;

import java.net.URL;

public class Channel {
	private String name;
	private URL url;
	private String display_name;
	private URL logo;
	private int _id;
	private String status;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public URL getUrl() {
		return url;
	}

	public void setUrl(URL url) {
		this.url = url;
	}

	public String getDisplay_name() {
		return display_name;
	}

	public void setDisplay_name(String display_name) {
		this.display_name = display_name;
	}

	public URL getLogo() {
		return logo;
	}

	public void setLogo(URL logo) {
		this.logo = logo;
	}

	@Override
	public String toString() {
		return String.format("Channel [name=%s, display_name=%s]", name, display_name);
	}

	public int get_id() {
		return _id;
	}

	public void set_id(int _id) {
		this._id = _id;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}
