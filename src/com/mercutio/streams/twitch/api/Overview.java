package com.mercutio.streams.twitch.api;

import java.net.URL;
import java.util.List;
import java.util.Map;

public class Overview {
	private List<Stream> streams;

	private Map<String, URL> _links;

	public List<Stream> getStreams() {
		return streams;
	}

	public void setStreams(List<Stream> streams) {
		this.streams = streams;
	}

	public Map<String, URL> get_links() {
		return _links;
	}

	public void set_links(Map<String, URL> _links) {
		this._links = _links;
	}

	@Override
	public String toString() {
		return String.format("Overview [streams=%s, _links=%s]", streams, _links);
	}

}
