package com.mercutio.streams.twitch.api;

import java.net.URL;
import java.util.Map;

public class Stream {

	private int viewers;
	private String game;
	
	/**
	 * Has multiple resolutions:
	 * <ul>
	 * <li>"medium" : "http://static-cdn.jtvnw.net/previews-ttv/live_user_garenatw-320x200.jpg",
	 * <li>"small" : "http://static-cdn.jtvnw.net/previews-ttv/live_user_garenatw-80x50.jpg",
	 * <li>"large" : "http://static-cdn.jtvnw.net/previews-ttv/live_user_garenatw-640x400.jpg",
	 * <li>"template" : "http://static-cdn.jtvnw.net/previews-ttv/live_user_garenatw-{width}x{height}.jpg"
	 * 
	 */
	private Map<String, URL> preview;

	private Channel channel;

	public int getViewers() {
		return viewers;
	}

	public void setViewers(int viewers) {
		this.viewers = viewers;
	}

	/**
	 * 
	 * @return "Others" if game is null, game otherwise
	 */
	public String getGame() {
		if (game == null)
			return "Others";
		return game;
	}

	public void setGame(String game) {
		this.game = game;
	}

	public Channel getChannel() {
		return channel;
	}

	public void setChannel(Channel channel) {
		this.channel = channel;
	}

	@Override
	public String toString() {
		return String.format("Stream [viewers=%s, game=%s, channel=%s]", viewers, game, channel);
	}

	/**
	 * A map of different resultions to the location of the preview images.
	 * 
	 * Example:
	 * <ul>
	 * <li>"medium" : "http://static-cdn.jtvnw.net/previews-ttv/live_user_garenatw-320x200.jpg",
	 * <li>"small" : "http://static-cdn.jtvnw.net/previews-ttv/live_user_garenatw-80x50.jpg",
	 * <li>"large" : "http://static-cdn.jtvnw.net/previews-ttv/live_user_garenatw-640x400.jpg",
	 * <li>"template" : "http://static-cdn.jtvnw.net/previews-ttv/live_user_garenatw-{width}x{height}.jpg"
	 * @return
	 */
	public Map<String, URL> getPreview() {
		return preview;
	}

	public void setPreview(Map<String, URL> preview) {
		this.preview = preview;
	}

}
