CREATE TABLE [entries] (
  [id] INTEGER NOT NULL PRIMARY KEY, 
  [date] INTEGER NOT NULL, 
  [streamer] INTEGER NOT NULL REFERENCES [streamers]([id]), 
  [game] INTEGER NOT NULL REFERENCES [games]([id]), 
  [viewers] INTEGER NOT NULL);


CREATE TABLE [favoriteGames] (
  [id] INTEGER NOT NULL PRIMARY KEY CONSTRAINT [favoriteG] REFERENCES [games]([id]));


CREATE TABLE [favoriteStreamers] (
  [id] INTEGER NOT NULL PRIMARY KEY CONSTRAINT [favoriteS] REFERENCES [streamers]([id]));


CREATE TABLE [games] (
  [id] INTEGER NOT NULL PRIMARY KEY, 
  [name] TEXT NOT NULL);

CREATE UNIQUE INDEX [namesI] ON [games] ([name] ASC);


CREATE TABLE [streamers] (
  [id] INTEGER NOT NULL PRIMARY KEY, 
  [name] TEXT NOT NULL);

CREATE UNIQUE INDEX [streamersI] ON [streamers] ([name] ASC);


CREATE VIEW [byDate] AS 
SELECT datetime(date, 'unixepoch', 'localtime') as d, SUM(viewers) as total, round(avg(viewers)) as avg, count() from entries group by date order by date;


CREATE VIEW [byFavoriteGame] AS 
SELECT datetime(date, 'unixepoch', 'localtime') as d, game, name, SUM(viewers) AS total, count() 
FROM entries, games AS g
INNER JOIN favoriteGames
ON favoriteGames.id = game
WHERE g.id = game
GROUP BY date, game 
ORDER BY date, total DESC;


CREATE VIEW [byFavoriteStreamer] AS 
SELECT datetime(date, 'unixepoch', 'localtime') as d, game, g.name AS gName, streamer, f.name AS sName, viewers
FROM entries, games AS g, streamers AS f
INNER JOIN favoriteStreamers AS fS
ON fS.id = streamer
WHERE g.id = game AND
      f.id = streamer      
GROUP BY date;


CREATE VIEW [byGame] AS 
SELECT datetime(date, 'unixepoch', 'localtime') as d, game, name, SUM(viewers) AS total, count() 
FROM entries, games AS g 
WHERE g.id = game
GROUP BY date, game 
ORDER BY date, total DESC;


CREATE VIEW [dota] AS 
SELECT datetime(date, 'unixepoch', 'localtime') as d, SUM(viewers) AS total, count() 
FROM entries
WHERE game = (SELECT id FROM games WHERE name = "Dota 2")
GROUP BY date 
ORDER BY date;


CREATE VIEW [hearthstone] AS 
SELECT datetime(date, 'unixepoch', 'localtime') as d, SUM(viewers) AS total, count() 
FROM entries
WHERE game = (SELECT id FROM games WHERE name = "Hearthstone: Heroes of Warcraft")
GROUP BY date 
ORDER BY date;


CREATE VIEW [lowcountdates] AS 
SELECT datetime(date, 'unixepoch', 'localtime') as d, date, c from (SELECT date, count() as c from entries
 group by date)
 where c < 200;


