{
	"_links" : {
		"featured" : "https://api.twitch.tv/kraken/streams/featured",
		"summary" : "https://api.twitch.tv/kraken/streams/summary",
		"next" : "https://api.twitch.tv/kraken/streams?limit=100&offset=100",
		"self" : "https://api.twitch.tv/kraken/streams?limit=100&offset=0",
		"followed" : "https://api.twitch.tv/kraken/streams/followed"
	},
	"streams" : [{
			"preview" : {
				"medium" : "http://static-cdn.jtvnw.net/previews-ttv/live_user_garenatw-320x200.jpg",
				"small" : "http://static-cdn.jtvnw.net/previews-ttv/live_user_garenatw-80x50.jpg",
				"large" : "http://static-cdn.jtvnw.net/previews-ttv/live_user_garenatw-640x400.jpg",
				"template" : "http://static-cdn.jtvnw.net/previews-ttv/live_user_garenatw-{width}x{height}.jpg"
			},
			"_links" : {
				"self" : "https://api.twitch.tv/kraken/streams/garenatw"
			},
			"game" : "League of Legends",
			"channel" : {
				"_links" : {
					"subscriptions" : "https://api.twitch.tv/kraken/channels/garenatw/subscriptions",
					"videos" : "https://api.twitch.tv/kraken/channels/garenatw/videos",
					"editors" : "https://api.twitch.tv/kraken/channels/garenatw/editors",
					"stream_key" : "https://api.twitch.tv/kraken/channels/garenatw/stream_key",
					"follows" : "https://api.twitch.tv/kraken/channels/garenatw/follows",
					"features" : "https://api.twitch.tv/kraken/channels/garenatw/features",
					"chat" : "https://api.twitch.tv/kraken/chat/garenatw",
					"commercial" : "https://api.twitch.tv/kraken/channels/garenatw/commercial",
					"self" : "https://api.twitch.tv/kraken/channels/garenatw"
				},
				"game" : "League of Legends",
				"url" : "http://www.twitch.tv/garenatw",
				"status" : "Garena Taiwan",
				"teams" : [{
						"_links" : {
							"self" : "https://api.twitch.tv/kraken/teams/riotgames"
						},
						"logo" : "http://static-cdn.jtvnw.net/jtv_user_pictures/team-riotgames-team_logo_image-0e709d1f4da393ec-300x300.png",
						"created_at" : "2012-09-29T02:36:37Z",
						"info" : "http://www.lolesports.com\n",
						"banner" : null,
						"display_name" : "Riot Games",
						"name" : "riotgames",
						"updated_at" : "2013-02-02T18:53:04Z",
						"background" : "http://static-cdn.jtvnw.net/jtv_user_pictures/team-riotgames-background_image-6cd687b2405d9ee2.jpeg",
						"_id" : 417
					}
				],
				"logo" : "http://static-cdn.jtvnw.net/jtv_user_pictures/garenatw-profile_image-8e5f1c5a36977f96-300x300.png",
				"created_at" : "2012-02-24T07:03:33Z",
				"banner" : null,
				"display_name" : "GarenaTW",
				"mature" : null,
				"name" : "garenatw",
				"updated_at" : "2013-03-21T10:14:34Z",
				"background" : "http://static-cdn.jtvnw.net/jtv_user_pictures/garenatw-channel_background_image-4c572132875c9c1d.jpeg",
				"video_banner" : "http://static-cdn.jtvnw.net/jtv_user_pictures/garenatw-channel_offline_image-265fb604e1bda41b-640x360.png",
				"_id" : 28462004
			},
			"name" : "live_user_garenatw",
			"viewers" : 19822,
			"broadcaster" : "fme",
			"_id" : 5167243024
		}
    ]
}