package com.mercutio.streams.test.twitch;

import static org.junit.Assert.*;

import org.junit.Test;

import com.mercutio.streams.Quality;

public class QualityTest {
	
	@Test
	public void accessQualities() {
		assertSame(Quality.BEST, Quality.valueOf("BEST"));
		assertSame(Quality.BEST, Quality.values()[0]);

		assertEquals(0, Quality.BEST.ordinal());
	}
}
