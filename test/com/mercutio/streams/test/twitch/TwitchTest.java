package com.mercutio.streams.test.twitch;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import com.google.gson.Gson;
import com.mercutio.streams.twitch.TwitchConnector;
import com.mercutio.streams.twitch.api.Overview;

@RunWith(JUnit4.class)
public class TwitchTest {

	@Test
	public void parse() throws FileNotFoundException, IOException {
		try (BufferedReader f = new BufferedReader(new FileReader("test-files/streams.js"))) {
			Overview overview = new Gson().fromJson(f, Overview.class);
			// "featured" : "https://api.twitch.tv/kraken/streams/featured"
			assertEquals(new URL("https://api.twitch.tv/kraken/streams/featured"), overview.get_links().get("featured"));
			assertEquals(1, overview.getStreams().size());
			assertEquals("League of Legends", overview.getStreams().get(0).getGame());
		}
	}

	@Test
	public void connect() throws IOException, InterruptedException {
		new TwitchConnector().loadPage();
	}

}
